package com.hendisantika.springbootelasticsearch;

import com.hendisantika.springbootelasticsearch.entity.Commodity;
import com.hendisantika.springbootelasticsearch.service.CommodityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;

@SpringBootTest
class SpringbootElasticsearchApplicationTests {

    @Autowired
    private CommodityService commodityService;

    @Test
    public void contextLoads() {
        System.out.println(commodityService.count());
    }

    @Test
    public void testInsert() {
        Commodity commodity = new Commodity();
        commodity.setSkuId("1501009001");
        commodity.setName("Original sliced side pack (10 pieces)");
        commodity.setCategory("101");
        commodity.setPrice(880);
        commodity.setBrand("Good child");
        commodityService.save(commodity);

        commodity = new Commodity();
        commodity.setSkuId("1501009002");
        commodity.setName("Original single-sided packaging (6 pieces)");
        commodity.setCategory("101");
        commodity.setPrice(680);
        commodity.setBrand("Good child");
        commodityService.save(commodity);

        commodity = new Commodity();
        commodity.setSkuId("1501009004");
        commodity.setName("Genki Toast 850g");
        commodity.setCategory("101");
        commodity.setPrice(120);
        commodity.setBrand("Herbal");
        commodityService.save(commodity);
    }

    @Test
    public void testDelete() {
        Commodity commodity = new Commodity();
        commodity.setSkuId("1501009002");
        commodityService.delete(commodity);
    }

    @Test
    public void testGetAll() {
        Iterable<Commodity> iterable = commodityService.getAll();
        iterable.forEach(e -> System.out.println(e.toString()));
    }

    @Test
    public void testGetByName() {
        List<Commodity> list = commodityService.getByName("面包");
        System.out.println(list);
    }

    @Test
    public void testPage() {
        Page<Commodity> page = commodityService.pageQuery(0, 10, "切片");
        System.out.println(page.getTotalPages());
        System.out.println(page.getNumber());
        System.out.println(page.getContent());
    }


}
