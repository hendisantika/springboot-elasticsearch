package com.hendisantika.springbootelasticsearch.repository;

import com.hendisantika.springbootelasticsearch.entity.Commodity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elasticsearch
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/01/20
 * Time: 06.49
 */
@Repository
public interface CommodityRepository extends ElasticsearchRepository<Commodity, String> {

}