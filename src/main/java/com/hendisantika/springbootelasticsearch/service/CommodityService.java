package com.hendisantika.springbootelasticsearch.service;

import com.hendisantika.springbootelasticsearch.entity.Commodity;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elasticsearch
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/01/20
 * Time: 06.49
 */
public interface CommodityService {
//    long count();

//    Commodity save(Commodity commodity);

//    void delete(Commodity commodity);

//    Iterable<Commodity> getAll();

    List<Commodity> getByName(String name);

    Page<Commodity> pageQuery(Integer pageNo, Integer pageSize, String kw);

}
